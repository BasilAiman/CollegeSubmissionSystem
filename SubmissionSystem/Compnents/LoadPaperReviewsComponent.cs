﻿using Microsoft.AspNetCore.Mvc;
using SubmissionSystem.Data;
using SubmissionSystem.Models;
using SubmissionSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace SubmissionSystem.Controllers
{
    [ViewComponent(Name = "LoadPaperReviews")]
    public class LoadPaperReviewsComponent : ViewComponent
    {
        private IReviewRepository _reviewsRepo;
        private IUserRepository _usersRepo;
        public LoadPaperReviewsComponent(IReviewRepository ReviewsRepo, IUserRepository UsersRepo)
        {
            _reviewsRepo = ReviewsRepo;
            _usersRepo = UsersRepo;
        }
        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            try
            {
                var user = await _usersRepo.GetUserByNameAsync(User.Identity.Name);
                var reviews = await _reviewsRepo.GetAllReviewsForPaperAsync(id);
                var result = reviews.Where(x => x.ReviewerId == user.Id).OrderBy(x => x.Date).ToList();
                ViewBag.UserId = user.Id;

                

                return View(result);

            }
            catch (Exception ex)
            {
                return View("_Error");
            }



        }
    }
}
