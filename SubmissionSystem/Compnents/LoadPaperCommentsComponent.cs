﻿using Microsoft.AspNetCore.Mvc;
using SubmissionSystem.Data;
using SubmissionSystem.Models;
using SubmissionSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Controllers
{
    [ViewComponent(Name = "LoadPaperComments")]
    public class LoadPaperCommentsComponent : ViewComponent
    {
        private ICommentsRepository _commentRepo;
        private IUserRepository _usersRepo;
        public LoadPaperCommentsComponent(ICommentsRepository CommentRepo, IUserRepository UsersRepo)
        {
            _commentRepo = CommentRepo;
            _usersRepo = UsersRepo;
        }
        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            try
            {
                var comments = await _commentRepo.GetCommentsForPaperAsync(id);
                var result = new List<TrackCommentViewModels>();
                TrackCommentViewModels temp;
                ApplicationUser tempUser;
                foreach (var c in comments)
                {
                    temp = new TrackCommentViewModels(c);
                 
                    tempUser = await _usersRepo.GetUserById(c.SenderId);
                    temp.Sendername = tempUser.DisplayName;
                    result.Add(temp);
                    temp = null;
                    tempUser = null;                                       
                }
                return View(result);

            }
            catch (Exception ex)
            {
                return View("_Error");
            }



        }
    }
}
