﻿using Microsoft.AspNetCore.Mvc;
using SubmissionSystem.Data;
using SubmissionSystem.Models;
using SubmissionSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Controllers
{
    [ViewComponent(Name = "LoadLastPapers")]
    public class LoadLastPapersComponent : ViewComponent
    {
        private ICommentsRepository _commentRepo;
        private IUserRepository _usersRepo;
        private IPaperRepository _paperRepo;

        public LoadLastPapersComponent(ICommentsRepository CommentRepo, IUserRepository UsersRepo,IPaperRepository PaperRepo)
        {
            _commentRepo = CommentRepo;
            _usersRepo = UsersRepo;
            _paperRepo = PaperRepo;

        }
        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            try
            {
                var result = await _paperRepo.GetAllPapers();
                var model = result.Take(4);

                return View(result);

            }
            catch (Exception ex)
            {
                return View("_Error");
            }



        }
    }
}
