﻿using Microsoft.AspNetCore.Mvc;
using SubmissionSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Controllers
{
    [ViewComponent(Name = "MainMenu")]
    public class MainMenuComponent : ViewComponent
    {
        public MainMenuComponent()
        {

        }
        public async Task<IViewComponentResult> InvokeAsync(string Menu)
        {
            var list = new List<LinkViewModel>();

            if (Menu == "Main")
            {

                list.Add(new LinkViewModel()
                {
                    Action = "Index",
                    Controller = "Home",
                    Text = "Home"
                });
                list.Add(new LinkViewModel()
                {
                    Action = "About",
                    Controller = "Home",
                    Text = "About"

                });
                list.Add(new LinkViewModel()
                {
                    Action = "Index",
                    Controller = "Papers",
                    Text = "Discover Papers"
                });


                if (User.IsInRole("Editor"))
                    list.Add(new LinkViewModel()
                    {
                        Action = "Index",
                        Controller = "Editors",
                        Text = "Work In Progress",
                        HtmlAttributes = new
                        {
                        }
                    });

                else if (User.IsInRole("User"))
                    list.Add(new LinkViewModel()
                    {
                        Action = "Track",
                        Controller = "Papers",
                        Text = "Track your Papers",
                        HtmlAttributes = new
                        {
                        }
                    });


                return View(list);
            }
            else
            {
                //If a guest 
                if (!User.Identity.IsAuthenticated)
                {
                    list.Add(new LinkViewModel()
                    {
                        Action = "Register",
                        Controller = "Home",
                        Text = "Register"
                    });
                    list.Add(new LinkViewModel()
                    {
                        Action = "Login",
                        Controller = "Home",
                        Text = "Login"
                    });






                    return View(list);

                }

                //If Authenticated   
                if (User.IsInRole("User"))
                    list.Add(new LinkViewModel()
                    {
                        Action = "Submit",
                        Controller = "Papers",
                        Text = "Submit a Papper <i class=\"fa fa-plus\"></i>",
                        HtmlId = "link-submitpaper"

                    });

                else if (User.IsInRole("Editor"))
                    list.Add(new LinkViewModel()
                    {
                        Action = "Accept",
                        Controller = "Papers",
                        Text = "Accept a Papper <i class=\"fa fa-check\"></i>",
                        HtmlId = "link-acceptpapter"

                    });
                else if (User.IsInRole("Reviewer-1") || User.IsInRole("Reviewer-2") || User.IsInRole("Reviewer-3"))
                    list.Add(new LinkViewModel()
                    {
                        Action = "Papers",
                        Controller = "Reviewers",
                        Text = "Review your Pappers <i class=\"fa fa-check-square-o\"></i>",
                        HtmlId = "link-reivewpapter"

                    });


                list.Add(new LinkViewModel()
                {
                    Text = "Hello, " + User.Identity.Name
                });
                if (User.IsInRole("User"))
                    list.Add(new LinkViewModel()
                    {
                        Action = "Index",
                        Controller = "Profile",
                        Text = "Profile"
                    });
                list.Add(new LinkViewModel()
                {
                    Action = "Logout",
                    Controller = "Home",
                    Route = null,
                    Text = "Logout"
                });
                return View(list);

            }

        }
    }
}
