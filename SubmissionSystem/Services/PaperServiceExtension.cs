﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
namespace SubmissionSystem.Services
{
    public static class PaperServiceExtension
    {

        public static IList<Paper> SearchPapers(this IList<Paper> original, Func<Paper, bool> predictor)
        {
            return original.Where(predictor).OrderByDescending(x => x.SubmissionDate).ToList(); 
        }
    }
}
