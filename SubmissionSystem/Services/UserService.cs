﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using SubmissionSystem.Data;
using SubmissionSystem.Models;
using SubmissionSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace SubmissionSystem.Services
{
    public class UserService
    {
        private readonly IUserRepository _users;
        private readonly IRoleRepository _roles;
        private readonly FileService _files;

        private ModelStateDictionary _modelState;

        public UserService(IUserRepository userRepository, IRoleRepository roleRepository, FileService Files)
        {
            _users = userRepository;
            _roles = roleRepository;
            _files = Files;
        }
        public void InitializeModelState(ModelStateDictionary State)
        {
            _modelState = State;
        }
        public async Task<RegisterViewModel> GetUserByNameAsync(string name)
        {
            var user = await _users.GetUserByNameAsync(name);

            if (user == null)
            {
                return null;
            }

            var viewModel = new RegisterViewModel
            {
                UserName = user.UserName,
                Email = user.Email,
                DisplayName = user.DisplayName,
                Birthday = user.Age,
                WorkPlace = user.WorkPlace,
                JobTitle = user.JobTitle,

            };
            var userRoles = await _users.GetRolesForUserAsync(user);

            viewModel.SelectedRole = userRoles.Count() > 1 ?
                userRoles.FirstOrDefault() : userRoles.SingleOrDefault();
            return viewModel;
        }

        public async Task<bool> CreateAsync(RegisterViewModel model)
        {
            if (!_modelState.IsValid)
            {
                return false;
            }
            if (String.IsNullOrWhiteSpace(model.UserName))
            {
                _modelState.AddModelError(string.Empty, "The username can't be empty");
                return false;
            }


            var existingUser = await _users.GetUserByNameAsync(model.UserName);

            if (existingUser != null)
            {
                _modelState.AddModelError(string.Empty, "The user already exists!");
                return false;
            }

            if (string.IsNullOrWhiteSpace(model.Password))
            {
                _modelState.AddModelError(string.Empty, "You must type a password.");
                return false;
            }

            var newUser = new ApplicationUser
            {
                DisplayName = model.DisplayName,
                Email = model.Email,
                UserName = model.UserName,
                JobTitle = model.JobTitle,
                WorkPlace = model.WorkPlace
            };
            try
            {
                await _users.CreateAsync(newUser, model.Password);

                await _users.AddUserToRoleAsync(newUser, model.SelectedRole);
                if (model.Image != null && model.Image.Length > 0)
                {
                    string ext = model.Image.FileName.Split('.').Last();
                    await _files.SaveFile(model.Image,newUser.Id+"."+ext);
                }
            }
            catch (Exception x)
            {
                _modelState.AddModelError(string.Empty, x.Message);
                return false;
            }

            return true;
        }

        public async Task<bool> UpdateUser(RegisterViewModel model)
        {
            var user = await _users.GetUserByNameAsync(model.UserName);

            if (user == null)
            {
                _modelState.AddModelError(string.Empty, "The specified user does not exist.");
                return false;
            }

            if (!_modelState.IsValid)
            {
                return false;
            }

            if (!string.IsNullOrWhiteSpace(model.Password))
            {
                if (string.IsNullOrWhiteSpace(model.CurrentPassword))
                {
                    _modelState.AddModelError(string.Empty, "The current password must be supplied");
                    return false;
                }

                var passwordVerified = await _users.VerifyUserPassword(user, model.CurrentPassword);

                if (!passwordVerified)
                {
                    _modelState.AddModelError(string.Empty, "The current password does match our records.");
                    return false;
                }
                try
                {
                    await _users.ChangePasswordAsync(user, model.CurrentPassword, model.Password);
                }
                catch (Exception x)
                {
                    _modelState.AddModelError(string.Empty, x.Message);
                    return false;
                }

            }

            user.Email = model.Email;
            user.DisplayName = model.DisplayName;

            await _users.UpdateAsync(user);
            var roles = await _users.GetRolesForUserAsync(user);
            await _users.RemoveUserFromRolesAsync(user, roles.ToArray());
            await _users.AddUserToRoleAsync(user, model.SelectedRole);
            if (model.Image != null && model.Image.Length > 0)
            {
                string ext = model.Image.FileName.Split('.').Last();
                await _files.SaveFile(model.Image, user.Id + "." + ext);
            }

            return true;

        }
     

        public async Task<bool> DeleteAsync(string username)
        {
            var user = await _users.GetUserByNameAsync(username);

            if (user == null)
            {
                return false;
            }

            await _users.DeleteAsync(user);
            return true;
        }
    }
}