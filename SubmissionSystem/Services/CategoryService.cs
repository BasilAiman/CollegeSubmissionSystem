﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SubmissionSystem.Data;
using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Services
{
    public class CategoryService
    {
        private readonly ICategoryRepository _repo;
        private ModelStateDictionary _modelState;
        private FileService _fileService;

        public CategoryService(ICategoryRepository Repo, FileService Service)
        {
            _repo = Repo;

            _fileService = Service;
        }
        public void InitializeModelState(ModelStateDictionary State)
        {
            _modelState = State;
        }
        public async Task<IList<Category>> GetAll()
        {
            return await _repo.GetAllAsync();
        }
        public async Task<Category> GetSingle(int id)
        {
            try
            {
                return await _repo.GetSingleAsync(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public async Task<bool> Add(Category Cat, IFormFile file)
        {
            if (!_modelState.IsValid)
            {
                return false;
            }
            if (await _repo.CheckIfCategoryWithNameExist(Cat.Name))
            {
                _modelState.AddModelError(string.Empty, "A category with this name already exists");
                return false;
            }
            if (file == null)
            {
                _modelState.AddModelError(string.Empty, "Please upload file");
                return false;
            }
            try
            {
                var path = await _fileService.SaveFile(file, Cat.Name + file.FileName);
                Cat.ImgPath = path;
                await _repo.Post(Cat);
                return true;

            }
            catch (Exception ex)
            {
                _modelState.AddModelError(string.Empty, ex.Message);
                return false;

            }
        }
        public async Task<bool> Edit(int id, Category cat, IFormFile file)
        {
            if (!_modelState.IsValid)
            {
                return false;
            }
            if (await _repo.CheckIfCategoryWithNameExist(cat.Name))
            {
                _modelState.AddModelError(string.Empty, "A category with this name already exists");
                return false;
            }
            try
            {
                if (file != null)
                {
                    var path =await  _fileService.SaveFile(file,cat.Name+file.FileName);
                    cat.ImgPath = path;
                }
                await _repo.Edit(id, cat);
                return true;

            }
            catch (Exception ex)
            {
                _modelState.AddModelError(string.Empty, ex.Message);
                return false;

            }

        }
        public async Task<bool> Delete(int id)
        {
            try
            {
                await _repo.Delete(id);
                return true;
            }
            catch (Exception ex)
            {
                _modelState.AddModelError(string.Empty, "You try to delete a non exisiting item.");
                return false;
            }

        }
    }
}
