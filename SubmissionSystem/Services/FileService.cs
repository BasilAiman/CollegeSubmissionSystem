﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;

namespace SubmissionSystem.Services
{
    public class FileService
    {
        private IHostingEnvironment _environment;
        public FileService(IHostingEnvironment environment)
        {
            _environment = environment;
        }
        public async Task<string> SaveFile(IFormFile file, string newfilename)
        {
            var path = Path.Combine(_environment.WebRootPath, "uploads", newfilename);
            DeleteFile(path); // Ensure the there's no duplicates
            if (file.Length > 0)
            {
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            return "/uploads/" + newfilename;

        }
        public void DeleteFile(string filename)
        {
            var path = Path.Combine(_environment.WebRootPath, "uploads", filename);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
        }
        public FileStream GetFileStream(string filename)
        {
            var path = _environment.WebRootPath + filename;
            var stream = new FileStream(path, FileMode.Open);
            return stream;
        }
    }
}
