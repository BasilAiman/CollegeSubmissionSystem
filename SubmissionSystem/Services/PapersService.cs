﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SubmissionSystem.Models;
using SubmissionSystem.Data;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SubmissionSystem.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace SubmissionSystem.Services
{
    public class PapersService
    {
        private IPaperRepository _repo;
        private FileService _files;
        private ModelStateDictionary _modelState;
        private ICategoryRepository _categories;
        private ICommentsRepository _comments;
        private IReviewRepository _reviewRepo;

        public PapersService(IPaperRepository Repo,
            ICategoryRepository Categories,
            FileService files, ICommentsRepository Comments,
            IReviewRepository ReviewRepo)
        {
            _repo = Repo;
            _files = files;
            _categories = Categories;
            _comments = Comments;
            _reviewRepo = ReviewRepo;
        }
        public void InitializeModelState(ModelStateDictionary State)
        {
            _modelState = State;
        }

        public async Task<PaperViewModel> GetSinglePaperAsync(int id)
        {
            var paper = await _repo.GetSinglePaper(id);
            return new PaperViewModel(paper);


        }
        public async Task<IList<Paper>> GetAllPaperOfUser(string UserId)
        {
            return await _repo.GetAllPapersOfUser(UserId);

        }
        public async Task<IList<Paper>> GetAllAdminPapersAsync()
        {
            return await _repo.GetAllPapers();
        }
        public async Task<IList<Paper>> GetAllEditorPapersAsync(string id)
        {
            return await _repo.GetPapersForEditor(id);
        }

        public async Task<bool> ReviewPaper(int PaperId, string ReviewrId, string Comment, bool Accept)
        {
            try
            {
                if (string.IsNullOrEmpty(Comment))
                {
                    _modelState.AddModelError(string.Empty, "Comment can't be empty");
                    return false;
                }

                await _reviewRepo.Post(new Review()
                {
                    Date = DateTime.Now,
                    Accepted = Accept,
                    Comment = Comment,
                    PaperId = PaperId,
                    ReviewerId = ReviewrId

                });
                return true;
            }
            catch (Exception ex)
            {

                throw ex;

            }

        }

        public async Task<IList<Paper>> GetAllReviewerPapersAsync(string id)
        {
            return await _repo.GetPapersForReviewer(id);
        }
        public async Task<bool> AddPaperAsync(PaperViewModel model, string currentUserId)
        {

            if (!_modelState.IsValid)
            {
                return false;
            }


            //Check if the cateogry input is valid
            var enteredCategory = await _categories.GetCategoryByName(model.SelectedCategory);
            if (enteredCategory == null)
            {
                _modelState.AddModelError(string.Empty, "An error happened, please choose a category");
                return false;

            }
            //Check if the file is empty
            if (model.File == null)
            {

                _modelState.AddModelError("File", "You muse upload a paper");
                return false;
            }

            var FileSavedPath = await _files.SaveFile(model.File, currentUserId + model.File.FileName);

            Paper input = new Paper()
            {
                Publish = false,
                isTaken = false,
                CategoryId = enteredCategory.Id,
                Description = model.Description,
                FullTilte = model.FullTilte,
                ShortTitle = model.ShortTitle,
                SubmissionDate = DateTime.Now,
                UserId = currentUserId,
                Field = model.Field,
                FinalPath = FileSavedPath,
                Status = "NEW",
                Abstraction = model.Abstraction


            };

            await _repo.Post(input);
            return true;


        }
        public async Task<bool> EditPaperAsync(PaperViewModel model)
        {


            if (!_modelState.IsValid)
            {
                return false;
            }


            //Check if the cateogry input is valid
            var enteredCategory = await _categories.GetCategoryByName(model.SelectedCategory);
            if (enteredCategory == null)
            {
                _modelState.AddModelError(string.Empty, "An error happened, please choose a category");
                return false;

            }

            try
            {
                var currnetPaper = await _repo.GetSinglePaper(model.Id);
                currnetPaper.FullTilte = model.FullTilte;
                currnetPaper.ShortTitle = model.ShortTitle;
                currnetPaper.Description = model.Description;
                currnetPaper.CategoryId = enteredCategory.Id;
                await _repo.Edit(model.Id, currnetPaper);

            }
            catch (Exception ex)
            {
                _modelState.AddModelError(string.Empty, "Invalid operation");

                return false;
            }

            return true;


        }

        public async Task<bool> AssignToEditor(string EditorId, int PaperId)
        {
            try
            {
                var currnetPaper = await _repo.GetSinglePaper(PaperId);

                currnetPaper.EditorId = EditorId;
                currnetPaper.isTaken = true;
                await _repo.Edit(PaperId, currnetPaper);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                await _repo.Delete(id);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public async Task<EditorPaperListViewModel> GetPaperForEditorBasedOnStatus(string editorId)
        {
            var allData = await GetAllEditorPapersAsync(editorId);

            var result = new EditorPaperListViewModel();
            result.New = ExtractByStatus("NEW", allData);
            result.Published = ExtractByStatus("PUBLISHED", allData);
            result.UnderReview = ExtractByStatus("UNDER REVIEW", allData);
            result.WaitingEdit = ExtractByStatus("WAITING EDIT", allData);
            return result;
        }
        private List<Paper> ExtractByStatus(string status, IList<Paper> list)
        {
            return list.Where(x => x.Status == status).OrderByDescending(x => x.SubmissionDate).ToList();
        }


        public async Task<bool> AssignToReviewer(int PaperId, string RevieweOneId, string ReviewerTwoId, string ReviewrThreeId)
        {
            try
            {
                var paper = await _repo.GetSinglePaper(PaperId);


                paper.Status = "UNDER REVIEW";
                paper.LevelOneReviewer = RevieweOneId;
                paper.LevelTwoReviewer = ReviewerTwoId;
                paper.LevelThreeReviewer = ReviewrThreeId;

                await _repo.Edit(paper.Id, paper);
                await _comments.Post(new Comment()
                {
                    Date = DateTime.Now,
                    PaperId = PaperId,
                    SenderId = paper.EditorId,
                    CommentText = "Thank you, Your paper is being reviewed now. We will contact you for further information"
                });
                await _reviewRepo.Post(new Review()
                {
                    IsSystemMessage = true,
                    PaperId = PaperId,
                    Date = DateTime.Now,
                    ReviewerId = RevieweOneId,
                    Comment = "Hello there, This Paper was submitted to be reviewed by the system. Kindly find the attachment"
                });
                await _reviewRepo.Post(new Review()
                {
                    IsSystemMessage = true,
                    PaperId = PaperId,
                    Date = DateTime.Now,
                    ReviewerId = ReviewerTwoId,
                    Comment = "Hello there, This Paper was submitted to be reviewed by the system. Kindly find the attachment"
                });
                await _reviewRepo.Post(new Review()
                {
                    IsSystemMessage = true,
                    PaperId = PaperId,
                    Date = DateTime.Now,
                    ReviewerId = ReviewrThreeId,
                    Comment = "Hello there, This Paper was submitted to be reviewed by the system. Kindly find the attachment"
                });
                return true;

            }
            catch (Exception ex)
            {
                return false;

            }

        }
        public async Task<bool> PublishPaper(int PaperId)
        {
            try
            {

                var paper = await _repo.GetSinglePaper(PaperId);
                paper.Publish = true;
                paper.Status = "PUBLISHED";
                await _repo.Edit(paper.Id, paper);
                return true;


            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public async Task<bool> SendEditorCommentToUser(int PaperId, string EditorId, string comment, bool Accpet)
        {
            try
            {
                var paper = await _repo.GetSinglePaper(PaperId);
                if (Accpet)
                {
                    await PublishPaper(PaperId);
                    comment = "Congratulations, Your paper was approved by our reviewers. Please read the following comment by your Editor \n " + comment;
                }
                //If Not Accepted we Will Change the status of the paper to WAITING EDIT
                else
                {
                    paper.Status = "WAITING EDIT";
                    await _repo.Edit(paper.Id, paper);
                }
                await _comments.Post(new Comment()
                {
                    CommentText = comment,
                    Date = DateTime.Now,
                    PaperId = PaperId,
                    SenderId = EditorId,
                });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public async Task<bool> UserUpdatePaperFile(int? paperid, IFormFile file)
        {
            try
            {
                if (paperid == null)
                    return false;

                var paper = await _repo.GetSinglePaper(paperid.Value);
                paper.Status = "WAITING REVIEW";
                Guid g = Guid.NewGuid();
                string name = g + "-" + file.FileName;
                var final_path = await _files.SaveFile(file, name);
                paper.FinalPath = final_path;

                Review r1 = new Review()
                {
                    Accepted = false,
                    Comment = "This paper has been updated, Please check the updated version and put your review",
                    IsSystemMessage = true,
                    PaperId = paperid.Value,
                    Date = DateTime.Now,
                    ReviewerId = paper.LevelOneReviewer
                };
                Review r2 = new Review()
                {
                    Accepted = false,
                    Comment = "This paper has been updated, Please check the updated version and put your review",
                    IsSystemMessage = true,
                    PaperId = paperid.Value,
                    Date = DateTime.Now,
                    ReviewerId = paper.LevelTwoReviewer
                };
                Review r3 = new Review()
                {
                    Accepted = false,
                    Comment = "This paper has been updated, Please check the updated version and put your review",
                    IsSystemMessage = true,
                    PaperId = paperid.Value,
                    Date = DateTime.Now,
                    ReviewerId = paper.LevelThreeReviewer
                };
                Comment c = new Comment()
                {
                    CommentText = "Thanks for you submissions. Your paper will be reviewed and we will get back to you as soon as possible. Thanks for your patience",
                    PaperId = paper.Id,
                    Path = final_path,
                    SenderId = paper.EditorId,
                    Date = DateTime.Now
                };

                await _repo.Edit(paper.Id, paper);
                await _reviewRepo.Post(r1);
                await _reviewRepo.Post(r2);
                await _reviewRepo.Post(r3);
                await _comments.Post(c);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }


    }
}
