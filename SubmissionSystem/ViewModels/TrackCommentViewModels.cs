﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.ViewModels
{
    public class TrackCommentViewModels
    {
        public TrackCommentViewModels(Comment comment)
        {
            this.Id = comment.Id;
            this.CommentText = comment.CommentText;
            this.Date = comment.Date;
            this.Path = comment.Path;
            this.PaperId = comment.PaperId;
            this.ImgPath = "/uploads/" + comment.SenderId + ".jpg";
            this.EditorId = comment.Paper.EditorId;
            this.SenderId = comment.SenderId;
            this.PaperStatus = comment.Paper.Status;
        }

        public int Id { get; set; }
        public string CommentText { get; set; }
        public DateTime Date { get; set; }
        public string Path { get; set; }
        public string EditorId { get; set; }
        public string SenderId { get; set; }
        public string Sendername { get; set; }
        public int PaperId { get; private set; }
        public string ImgPath { get; private set; }
        public string PaperStatus { get; set; }
    }
}
