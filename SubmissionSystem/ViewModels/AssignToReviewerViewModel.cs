﻿using Microsoft.AspNetCore.Mvc.Rendering;
using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.ViewModels
{
    public class AssignToReviewerViewModel
    {
        public int Id { get; set; }

        public SelectList LevelOneList { get; set; }
        public SelectList LevelTwoList { get; set; }
        public SelectList LevelThreeList { get; set; }
        public string PaperTitle { get; set; }
        public string UserDisplayName { get; set; }

        public void LoadUsers(IEnumerable<ApplicationUser> LevelOne, IEnumerable<ApplicationUser> LevelTwo, IEnumerable<ApplicationUser> LevelThree) {
            LevelOneList = new SelectList(LevelOne, "Id", "DisplayName");
            LevelTwoList = new SelectList(LevelTwo, "Id", "DisplayName");
            LevelThreeList = new SelectList(LevelThree, "Id", "DisplayName");
        }


        public string UserNameOneId { get; set; }
        public string UserNameTwoId { get; set; }
        public string UserNameThreeId { get; set; }
    }
}
