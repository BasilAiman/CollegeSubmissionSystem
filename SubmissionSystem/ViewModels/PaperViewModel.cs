﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.ViewModels
{
    public class PaperViewModel
    {
        public PaperViewModel()
        {

        }
        public PaperViewModel(Paper paper)
        {
            this.SelectedCategory = paper.Category.Name;
            this.Abstraction = paper.Abstraction;
            this.Description = paper.Description;
            this.Field = paper.Field;
            this.FullTilte = paper.FullTilte;
            this.ShortTitle = paper.ShortTitle;
           
            
        }
        public int Id { get; set; }

        [Display(Name = "Selected Category")]
        [Required]
        public string SelectedCategory { get; set; }

        [Display(Name = "Selected Editor")]
        public string SelectedEditor { get; set; }

        [Display(Name = "Field")]
        [Required]
        public string Field { get; set; }
        [Display(Name = "Full Tilte")]
        [Required]
        public string FullTilte { get; set; }
        [Display(Name = "Short Title")]
        [Required]
        [MaxLength(100)]
        public string ShortTitle { get; set; }

        [Display(Name = "Abstraction")]
        [Required]
        [DataType(DataType.MultilineText)]
        [MinLength(200)]
        public string Abstraction { get; set; }

        [Display(Name = "Content")]
        [Required]
        [DataType(DataType.MultilineText)]
        [MinLength(200)]
        public string Description { get; set; }

        [Display(Name = "Upload your paper")]
        public IFormFile File { get; set; }



        private readonly List<string> _categories = new List<string>();
        public IEnumerable<SelectListItem> Categories
        {
            get { return new SelectList(_categories); }
        }
        public void LoadUserCategories(IEnumerable<Category> cats)
        {
            _categories.AddRange(cats.Select(x => x.Name));
        }



        private readonly List<string> _editors = new List<string>();
        public IEnumerable<SelectListItem> Editors
        {
            get { return new SelectList(_editors); }
        }
        public void LoadUserEditor(IEnumerable<ApplicationUser> cats)
        {
            _editors.AddRange(cats.Select(x => x.DisplayName));
        }

    }
}
