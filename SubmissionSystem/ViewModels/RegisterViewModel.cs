﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.ViewModels
{
    public class RegisterViewModel
    {
        public string UserName { get; set; }

        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Current Password")]
        public string CurrentPassword { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password")]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Work Place")]
        public string WorkPlace { get; set; }

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name ="Profile Picture")]
        public IFormFile Image { get; set; }


        [Display(Name = "Role")]
        public string SelectedRole { get; set; }

        private readonly List<string> _roles = new List<string>();
        public IEnumerable<SelectListItem> Roles
        {
            get
            {
                return new SelectList(_roles);
            }

        }
        public void LoadUserRoles(IEnumerable<IdentityRole> roles)
        {
            _roles.AddRange(roles.Select(r => r.Name));
        }


    }
}
