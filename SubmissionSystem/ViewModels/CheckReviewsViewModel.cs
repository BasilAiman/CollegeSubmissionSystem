﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.ViewModels
{
    public class CheckReviewsViewModel
    {
        public IList<Review> LevelOneReivews { get; set; }
        public IList<Review> LevelTwoReviews { get; set; }
        public IList<Review> LevelThreeReviews { get; set; }
        public Paper Paper { get; set; }
    }
}
