﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.ViewModels
{
    public class ProfileViewModel
    {
        public ApplicationUser User { get; set; }
        public IEnumerable<Paper> Papers { get; set; }
        
    }
}
