﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.ViewModels
{
    public class EditorPaperListViewModel
    {
        public IList<Paper> New { get; set; }
        public IList<Paper> UnderReview { get; set; }
        public IList<Paper> WaitingEdit { get; set; }
        public IList<Paper> Published { get; set; }

    }
}
