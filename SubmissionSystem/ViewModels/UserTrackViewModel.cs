﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.ViewModels
{
    public class UserTrackViewModel
    {

        public int Id { get; set; }
        public IList<PaperTrackForUserViewModel> Papers { get; set; }



        public void AddPapers(IList<Paper> list,int papperId)
        {
            Papers = new List<PaperTrackForUserViewModel>();
            for (int i = 0; i < list.Count; i++)
            {
                var originalPaper = list[i];
                this.Papers.Add(new PaperTrackForUserViewModel() {
                    Id = originalPaper.Id,
                    FullTitle = originalPaper.FullTilte,
                    SubmissionDate = originalPaper.SubmissionDate,
                    Status = originalPaper.Status,
                    Selected = (papperId == originalPaper.Id)
                });
            }
        }
    }
    public class ReviewerTrackViewModel : UserTrackViewModel { }
    //This class is used for populating the data in the tracking
    public class PaperTrackForUserViewModel {
        public int Id { get; set; }
        public string FullTitle { get; set; }
        public bool Selected { get; set; } = false;
        public DateTime SubmissionDate { get; set; }
        public string Status { get; set; }
      
    }
}
