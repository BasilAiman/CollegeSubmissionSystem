﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.ViewModels
{
    public class LinkViewModel
    {
        public string Text { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string HtmlId { get; set; }
        public string HtmlClass { get; set;}
        public string Area { get; set; }
        public object Route { get; set; }
        public object HtmlAttributes { get; set; }
    }
}
