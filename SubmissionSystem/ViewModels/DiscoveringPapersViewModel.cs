﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.ViewModels
{
    public class DiscoveringPapersViewModel
    {
        public int? currentId { get; set; }
        public IList<Paper> Papers { get; set; }
        public IList<Category> Categories { get; set; }
    }
}
