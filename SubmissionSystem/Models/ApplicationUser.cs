﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string DisplayName { get; set; }
        public DateTime Age { get; set; }
        public string JobTitle { get; set; }
        public string WorkPlace { get; set; }

        public virtual ICollection<FollowCategory> FollowingCategories { get; set; }
    }
}
