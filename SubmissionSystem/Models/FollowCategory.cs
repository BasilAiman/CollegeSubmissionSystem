﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Models
{
    public class FollowCategory
    {
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}
