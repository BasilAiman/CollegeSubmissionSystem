﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Models
{
    public class Paper
    {
        [Key]
        public int Id { get; set; }

        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public bool Publish { get; set; } //Indicate if Published to the library
        public bool isTaken { get; set; } //Indicate if the Editor is taking it
        public string Status { get; set; } //Indicate The Status It Should be NEW - PUBLISHED - UNDER REVIEW - WAITING EDIT


        public string Field { get; set; }
        public string RevisionCommet { get; set; }
        public DateTime SubmissionDate { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        public string EditorId { get; set; }
        [ForeignKey("EditorId")]
        public ApplicationUser Editor { get; set; }

        public string LevelOneReviewer { get; set; }
        public string LevelTwoReviewer { get; set; }
        public string LevelThreeReviewer { get; set; }



        public string FullTilte { get; set; }
        public string ShortTitle { get; set; }
        public string Description { get; set; }
        public string Abstraction { get; set; }
        public string FinalPath { get; set; }
        public int DownloadCounter { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }


    }
}
