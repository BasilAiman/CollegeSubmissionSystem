﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Models
{
    public class Review
    {
        [Key]
        public int Id { get; set; }
        public string ReviewerId { get; set; }    
        public string Comment { get; set; }
        public bool Accepted { get; set; }
        public bool IsSystemMessage { get; set; } //This to mark if the post is a system message
        public DateTime Date { get; set; }
        public int PaperId { get; set; }
        [ForeignKey("PaperId")]
        public virtual Paper Paper { get; set; }




    }
}
