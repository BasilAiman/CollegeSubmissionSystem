﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        public string CommentText { get; set; }
        public DateTime Date { get; set; }

        public string SenderId { get; set; }

        public int PaperId { get; set; }
        [ForeignKey("PaperId")]
        public virtual Paper Paper { get; set; }

        public string Path { get; set; } // The File Path, Only Added in by the User
    }
}
