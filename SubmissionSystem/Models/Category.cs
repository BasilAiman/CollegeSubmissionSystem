﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Models
{
    public class Category
    {
        public Category()
        {
            Papers = new HashSet<Paper>();
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [Required]
        public string Code { get; set; }
        public string ImgPath { get; set; }
        [NotMapped]
        public IFormFile img {get;set;}

        public virtual ICollection<Paper> Papers { get; set; }
        public virtual ICollection<FollowCategory> FollowingUsers { get; set; }
    }
}
