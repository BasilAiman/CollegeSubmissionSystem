using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SubmissionSystem.Data;
using SubmissionSystem.Models;
using SubmissionSystem.Services;
using SubmissionSystem.ViewModels;

namespace SubmissionSystem.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("[area]/Papers")]
    public class PapersController : Controller
    {
        private readonly PapersService _service;
        private readonly IUserRepository _users;
        private readonly ICategoryRepository _categories;
        public PapersController(PapersService Service, IUserRepository UsersRepo, ICategoryRepository Cats)
        {
            _service = Service;
            _users = UsersRepo;
            _categories = Cats;
        }
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Index()
        {

            return View(await _service.GetAllAdminPapersAsync());



            IList<Paper> result = null;
            var CurrentUser = await _users.GetUserByNameAsync(User.Identity.Name);
            if (User.IsInRole("Admin"))
            {
                result = await _service.GetAllAdminPapersAsync();
            }
            if (User.IsInRole("Editor"))
            {
                result = await _service.GetAllEditorPapersAsync(CurrentUser.Id);

            }
            if (User.IsInRole("Reviewer"))
            {
                result = await _service.GetAllReviewerPapersAsync(CurrentUser.Id);

            }
            return View(result);
        }

        [HttpGet]
        [Route("Create")]
        public async Task<IActionResult> Create()
        {
            _service.InitializeModelState(ModelState);

            var model = new PaperViewModel();
            model.LoadUserCategories(await _categories.GetAllAsync());
            return View(model);
        }
        [HttpPost]
        [Route("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PaperViewModel model)
        {
            _service.InitializeModelState(ModelState);
            //Todo: Change the userID
            bool completed = await _service.AddPaperAsync(model, "00b8792d-3190-4393-a71c-5e1d1638b1df");
            if (completed)
                return RedirectToAction("index");

            model.LoadUserCategories(await _categories.GetAllAsync());
            return View(model);


        }
        [HttpGet]
        [Route("Edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {

            _service.InitializeModelState(ModelState);
            try
            {
                var model = await _service.GetSinglePaperAsync(id);

                model.LoadUserCategories(await _categories.GetAllAsync());
                model.LoadUserEditor(await _users.GetAllUsersInRoleAsync("Editor"));
                return View(model);
            }
            catch (KeyNotFoundException eX)
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, PaperViewModel model)
        {
            if (id != model.Id)
                return BadRequest();

            _service.InitializeModelState(ModelState);
            //Todo: Change the userID
            bool completed = await _service.EditPaperAsync(model);
            if (completed) 
                return RedirectToAction("index");

            model.LoadUserCategories(await _categories.GetAllAsync());
            model.LoadUserEditor(await _users.GetAllUsersInRoleAsync("Editor"));

            return View(model);


        }
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var paper = await _service.GetSinglePaperAsync(id);
                return View(paper);

            }
            catch (KeyNotFoundException eX)
            {
                return NotFound();
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Route("Delete")]

        public async Task<IActionResult> Delete(int id)
        {
            _service.InitializeModelState(ModelState);

            var completed = await _service.Delete(id);
            if (completed)
                return Ok();

            return NotFound();



        }
    }
}