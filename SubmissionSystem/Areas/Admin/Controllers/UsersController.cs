using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SubmissionSystem.Data;
using SubmissionSystem.ViewModels;
using SubmissionSystem.Services;

namespace SubmissionSystem.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("[area]/Users")]
    public class UsersController : Controller
    {
        private IUserRepository _repo;
        private IRoleRepository _roles;
        private UserService _service;
        public UsersController(IUserRepository Repo, IRoleRepository Roles, UserService Services)
        {
            _repo = Repo;
            _roles = Roles;
            _service = Services;
        }
        [Route("")]
        public async Task<IActionResult> Index()
        {
            var result = await _repo.GetAllUsersAsync();
            return View(result);
        }
        [Route("Create")]
        [HttpGet]
        public async Task<IActionResult> Create()
        {



            var model = new RegisterViewModel();
            model.LoadUserRoles(await _roles.GetAllRolesAsync());
            return View(model);
        }
        [Route("Create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RegisterViewModel model)
        {
            _service.InitializeModelState(this.ModelState);
            bool completed = await _service.CreateAsync(model);

            if (completed)
            {
                return RedirectToAction("Index");
            }
            model.LoadUserRoles(await _roles.GetAllRolesAsync());

            return View(model);
        }
        [HttpGet]
        [Route("Edit/{username}")]
        public async Task<IActionResult> Edit(string username)
        {
            if (username == null)
                return RedirectToAction("Index");

            var model = await _service.GetUserByNameAsync(username);
            model.LoadUserRoles(await _roles.GetAllRolesAsync());
            if (model == null)
                return new NotFoundResult();
            return View(model);
        }
        [HttpPost]
        [Route("Edit/{username}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string username, RegisterViewModel model)
        {

            if (!string.Equals(username, model.UserName))
                return BadRequest();

            _service.InitializeModelState(this.ModelState);
            bool completed = await _service.UpdateUser(model);
            if (completed)
            {
                return RedirectToAction("Index");
            }
            model.LoadUserRoles(await _roles.GetAllRolesAsync());
            return View(model);
        }
        [HttpGet]
        [Route("Details/{username}")]
        public async Task<IActionResult> Details(string username) {
            var user = await _service.GetUserByNameAsync(username);
            return View(user);

        }


        [Route("Delete/{username}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string username)
        {
            bool res = await _service.DeleteAsync(username);
            if (res)
                return Ok();
            else
                return BadRequest();

        }

    }
}