using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SubmissionSystem.Data;
using SubmissionSystem.Models;
using SubmissionSystem.Services;
using Microsoft.AspNetCore.Http;

namespace SubmissionSystem.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("[area]/Categories")]
    public class CategoriesController : Controller
    {
        private readonly CategoryService _service;


        public CategoriesController(CategoryService serivce)
        {
            _service = serivce;
        }

        // GET: Admin/Categories
        [Route("")]
        public async Task<IActionResult> Index()
        {
            return View(await _service.GetAll());
        }

        // GET: Admin/Categories/Details/5
        [Route("Details")]
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _service.InitializeModelState(ModelState);

            var category = await _service.GetSingle(id); ;
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // GET: Admin/Categories/Create
        [Route("Post")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Post")]
        public async Task<IActionResult> Create(Category category)
        {
            _service.InitializeModelState(this.ModelState);

            bool completed = await _service.Add(category, category.img);
            if (!completed)
            {
                return View(category);
            }

            return RedirectToActionPermanent("Index");
        }

        // GET: Admin/Categories/Edit/5
        [Route("Edit")]

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _service.InitializeModelState(ModelState);

            var category = await _service.GetSingle(id.Value);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        // POST: Admin/Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Edit")]

        public async Task<IActionResult> Edit(int id, Category category)
        {
            if (id != category.Id)
            {
                return NotFound();
            }


            _service.InitializeModelState(ModelState);

            bool completed = await _service.Edit(id, category,category.img);

            if (!completed)

                return View(category);
            else return RedirectToAction("Index");

        }


        // GET: Admin/Categories/Delete/5     
        [Route("Delete")]

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _service.InitializeModelState(ModelState);

            var category = await _service.GetSingle(id.Value);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // POST: Admin/Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Route("Delete")]

        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _service.InitializeModelState(ModelState);

            try
            {
                var completed = await _service.Delete(id);
                if (completed)
                    return RedirectToAction("Index");

                return View(await _service.GetSingle(id));

                        
            }
            catch (Exception ex) {
                return View(await _service.GetSingle(id));

            }

        }

      
    }
}
