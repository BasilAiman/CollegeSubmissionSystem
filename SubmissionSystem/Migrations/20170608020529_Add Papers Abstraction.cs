﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SubmissionSystem.Migrations
{
    public partial class AddPapersAbstraction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Approved",
                table: "Papers",
                newName: "isTaken");

            migrationBuilder.AddColumn<string>(
                name: "Abstraction",
                table: "Papers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Publish",
                table: "Papers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Papers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Abstraction",
                table: "Papers");

            migrationBuilder.DropColumn(
                name: "Publish",
                table: "Papers");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Papers");

            migrationBuilder.RenameColumn(
                name: "isTaken",
                table: "Papers",
                newName: "Approved");
        }
    }
}
