﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SubmissionSystem.Migrations
{
    public partial class AddingapprovmenttoReviewsandReviewerIdstopaper : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Accepted",
                table: "Reviews",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "LevelOneReviewer",
                table: "Papers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LevelThreeReviewer",
                table: "Papers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LevelTwoReviewer",
                table: "Papers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Accepted",
                table: "Reviews");

            migrationBuilder.DropColumn(
                name: "LevelOneReviewer",
                table: "Papers");

            migrationBuilder.DropColumn(
                name: "LevelThreeReviewer",
                table: "Papers");

            migrationBuilder.DropColumn(
                name: "LevelTwoReviewer",
                table: "Papers");
        }
    }
}
