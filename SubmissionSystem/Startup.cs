﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SubmissionSystem.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using SubmissionSystem.Data;
using Microsoft.EntityFrameworkCore;
using SubmissionSystem.Services;
using Microsoft.AspNetCore.Http.Features;

namespace SubmissionSystem
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ICommentsRepository, CommentsRepository>();
            services.AddScoped<IPaperRepository, PaperRepository>();
            services.AddScoped<IReviewRepository, ReviewsRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped(typeof(UserService));
            services.AddScoped(typeof(CategoryService));
            services.AddScoped(typeof(FileService));
            services.AddScoped(typeof(PapersService));
            
           


            services.AddDbContext<SystemDbContext>((option) => option.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<SystemSeedDbData>();
            services.AddIdentity<ApplicationUser, IdentityRole>((option) =>
            {
                option.Password.RequireDigit = false;
                option.Password.RequireLowercase = false;
                option.Password.RequireNonAlphanumeric = false;
                option.Password.RequireUppercase = false;

            })
             .AddSignInManager<SystemSigninManager>()
             .AddEntityFrameworkStores<SystemDbContext>();
            services.AddMvc();
            services.Configure<FormOptions>((options) =>
            {
                options.ValueLengthLimit = int.MaxValue;
                options.MultipartBodyLengthLimit = int.MaxValue;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, SystemSeedDbData seed)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();

                seed.EnsureSeedData().Wait();


            }


            app.UseStaticFiles();

            app.UseIdentity();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "adminArea",
                    template: "{area:exists}/{controller=Default}/{action=Index}");


                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

            });

        }
    }
}
