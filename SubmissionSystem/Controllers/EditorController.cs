using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SubmissionSystem.Data;
using SubmissionSystem.Services;
using SubmissionSystem.ViewModels;

namespace SubmissionSystem.Controllers
{
    [Route("Editors")]
    public class EditorsController : Controller
    {
        private readonly PapersService _service;
        private IUserRepository _usersRepo;
        private ICategoryRepository _catRepo;
        private IPaperRepository _paperRepo;
        private IReviewRepository _reviewsRepo;
        public EditorsController(PapersService Serivce,
            IUserRepository UsersRepo,
            ICategoryRepository Catsrepo,
            IPaperRepository PaperRepo
            , IReviewRepository ReviewsRepo)
        {
            _service = Serivce;
            _usersRepo = UsersRepo;
            _catRepo = Catsrepo;
            _paperRepo = PaperRepo;
            _reviewsRepo = ReviewsRepo;
        }

        [Route("")]
        public async Task<IActionResult> Index()
        {
            var user = await _usersRepo.GetUserByNameAsync(User.Identity.Name);
            var result = await _service.GetPaperForEditorBasedOnStatus(user.Id);
            return View(result);
        }
        [Route("AssignToReviewer/{id}")]
        public async Task<IActionResult> AssignToReviewer(int id)
        {
            var model = new AssignToReviewerViewModel();
            var paper = await _paperRepo.GetSinglePaper(id);
            var levelOne = await _usersRepo.GetAllUsersInRoleAsync("Reviewer-1");
            var levelTwo = await _usersRepo.GetAllUsersInRoleAsync("Reviewer-2");
            var levelThree = await _usersRepo.GetAllUsersInRoleAsync("Reviewer-3");
            model.LoadUsers(levelOne, levelTwo, levelThree);
            model.Id = id;
            model.PaperTitle = paper.FullTilte;
            model.UserDisplayName = paper.User.DisplayName;
            return View(model);
        }
        [Route("AssignToReviewer/{id}")]
        [HttpPost]
        public async Task<IActionResult> AssignToReviewer(AssignToReviewerViewModel model, int id)
        {

            bool done = await _service.AssignToReviewer(id, model.UserNameOneId, model.UserNameTwoId, model.UserNameThreeId);
            if (done)
                return RedirectToAction("Index");


            return BadRequest();
        }

        [Route("CheckReviews/{id:int}")]
        public async Task<IActionResult> CheckReviews(int id)
        {
            var paper = await _paperRepo.GetSinglePaper(id);
            var model = new CheckReviewsViewModel();
            model.LevelOneReivews = await _reviewsRepo.GetAllReviewsForPaperByReviewsAsync(id, paper.LevelOneReviewer, true);
            model.LevelTwoReviews = await _reviewsRepo.GetAllReviewsForPaperByReviewsAsync(id, paper.LevelTwoReviewer, true);
            model.LevelThreeReviews = await _reviewsRepo.GetAllReviewsForPaperByReviewsAsync(id, paper.LevelThreeReviewer, true);
            model.Paper = paper;


            return View(model);
        }
        [HttpGet]
        [Route("Publish/{id}")]
        public async Task<IActionResult> Publish(int id)
        {
            await _service.PublishPaper(id);
            return RedirectToAction("Index");

        }
        [HttpPost]
        [Route("CheckReviews/{id:int}")]
        public async Task<IActionResult> CheckReviews(int id, string comment, bool Accept)
        {
            var user = await _usersRepo.GetUserByNameAsync(User.Identity.Name);
            if (string.IsNullOrWhiteSpace(comment))
                return BadRequest();

            var completed = await _service.SendEditorCommentToUser(id, user.Id, comment, Accept);
            if (completed)
                return RedirectToAction("Index");

            return BadRequest();

        }
    }
}