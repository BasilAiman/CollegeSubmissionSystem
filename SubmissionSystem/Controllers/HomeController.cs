using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SubmissionSystem.Data;
using SubmissionSystem.ViewModels;
using SubmissionSystem.Services;
using SubmissionSystem.Models;

namespace SubmissionSystem.Controllers
{
    [Route("/")]
    public class HomeController : Controller
    {
        private IUserRepository _repo;
        private IRoleRepository _roles;
        private IPaperRepository _paperRepo;
        private UserService _service;
        private SystemSigninManager _siginManager;
        private ICategoryRepository _catRepo;

        public HomeController(IUserRepository Repo,ICategoryRepository CatRepo ,IRoleRepository Roles, UserService Services, SystemSigninManager signManager, IPaperRepository PaperRepo)
        {
            _repo = Repo;
            _roles = Roles;
            _service = Services;
            _siginManager = signManager;
            _paperRepo = PaperRepo;
            _catRepo = CatRepo;
        }


        [Route("")]
        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var cats = await _catRepo.GetAllAsync();
                return View("IndexAuth", cats);
            }
            return View();
        }

        [Route("About")]
        public async Task<IActionResult> About()
        {
            ViewBag.Editors = await  _repo.GetAllEditors();
            ViewBag.Reviewers =await  _repo.GetAllReviewer();

           return  View();
        }

        [Route("Welcome")]
        public IActionResult Welcome() => View();

        [Route("Profile")]
        public async Task<IActionResult> Profile()
        {
            var model = new ProfileViewModel();
            model.User = await _repo.GetUserByNameAsync(User.Identity.Name);
            model.Papers = await _paperRepo.GetAllPapersOfUser(model.User.Id);
            return View(model);
        }

        [HttpPost]
        [Route("Search")]
        public async Task<IActionResult> Search(string keywords)
        {
            var all_papers = await _paperRepo.GetAllPapers();
            var result = new List<Paper>();
            foreach (var x in all_papers)
            {
                if (x.FullTilte != null)
                {
                    if (x.FullTilte.Contains(keywords))
                        result.Add(x);

                }
                if (x.Description != null)
                {
                    if (x.Description.Contains(keywords))
                        result.Add(x);
                }
                if (x.Abstraction != null)
                {
                    if (x.Abstraction.Contains(keywords))
                        result.Add(x);
                }

            }
            ViewBag.words = keywords;
            return View(result);

        }
        [Route("Register")]
        [HttpGet]
        public IActionResult Register()
        {

            var model = new RegisterViewModel();
            model.Birthday = DateTime.Now;
            return View(model);
        }
        [Route("Register")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {

            model.SelectedRole = "User";
            _service.InitializeModelState(this.ModelState);
            bool completed = await _service.CreateAsync(model);

            if (completed)
            {
                var user = await _repo.GetUserByNameAsync(model.UserName);
                await _siginManager.PasswordSignInAsync(user, model.ConfirmPassword, true, false);
                return RedirectToAction("Welcome");
            }

            return View(model);
        }
        [HttpGet]
        [Route("Category/{id}")]
        public async Task<IActionResult> Category(int? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var model = await _catRepo.GetSingleAsync(id.Value);
            int Papers = model.Papers.Count();
            int users = model.Papers.Select(x => x.User).Distinct().Count();
            ViewBag.PapersCount = Papers;
            ViewBag.UsersCount = users;
            return View(model);
        }
        [HttpGet]
        [Route("Login")]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            //Sign in
            var user = await _repo.GetUserByNameAsync(model.Username);
            if (user != null)
            {
                var result = await _siginManager.PasswordSignInAsync(user, model.Password, model.isPersistent, false);
                if (result.Succeeded)
                    return RedirectToActionPermanent("Index");
            }
            ModelState.AddModelError(string.Empty, "The username or the password are wrong.");
            return View(model);



        }
        [HttpGet]
        [Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {

                await _siginManager.SignOutAsync();

            }
            return RedirectToAction("Index");
        }





    }
}