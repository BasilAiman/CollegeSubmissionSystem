﻿using Microsoft.AspNetCore.Mvc;
using SubmissionSystem.Data;
using SubmissionSystem.Services;
using SubmissionSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Controllers
{
    [Route("Reviewers")]
    public class ReviewersController : Controller
    {
        private IReviewRepository _reviewRepo;
        private IPaperRepository _paperRepo;
        private IUserRepository _userRepo;
        private PapersService _paperService;
        public ReviewersController(IReviewRepository ReviewRepo,
            IPaperRepository PaperRepo,
            IUserRepository UserRepo,
            PapersService PaperService)
        {
            _reviewRepo = ReviewRepo;
            _paperRepo = PaperRepo;
            _userRepo = UserRepo;
            _paperService = PaperService;
        }
        [Route("Papers/{paperid:int}")]
        [Route("Papers")]
        public async Task<IActionResult> Papers(int? paperid)
        {
            //Load the papers for the logged in reviewrs

            var user = await _userRepo.GetUserByNameAsync(User.Identity.Name);
            var papers = await _paperRepo.GetPapersForReviewer(user.Id);
            var model = new ReviewerTrackViewModel();
            if (paperid == null)
                paperid = 0;
            model.Id = paperid.Value;
            model.AddPapers(papers, paperid.Value);
            return View(model);
        }
        [Route("Papers/{paperid:int}")]
        [Route("Papers")]
        [HttpPost]
        public async Task<IActionResult> Papers(string comment, bool Accept, int paperid)
        {
            _paperService.InitializeModelState(this.ModelState);
            var user = await _userRepo.GetUserByNameAsync(User.Identity.Name);
            bool completed = await _paperService.ReviewPaper(paperid, user.Id, comment, Accept);

            return RedirectToAction("Papers");


        }
    }
}
