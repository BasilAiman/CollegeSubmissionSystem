using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SubmissionSystem.ViewModels;
using SubmissionSystem.Services;
using SubmissionSystem.Data;
using Microsoft.AspNetCore.Http;

namespace SubmissionSystem.Controllers
{
    [Route("Papers")]
    public class PapersController : Controller
    {
        private readonly PapersService _service;
        private readonly FileService _fileService;
        private IUserRepository _usersRepo;
        private ICategoryRepository _catRepo;
        private IPaperRepository _paperRepo;
        public PapersController(PapersService Serivce, IUserRepository UsersRepo, ICategoryRepository Catsrepo, IPaperRepository PaperRepo, FileService FileService)
        {
            _service = Serivce;
            _usersRepo = UsersRepo;
            _catRepo = Catsrepo;
            _paperRepo = PaperRepo;
            _fileService = FileService;
        }

        [HttpGet]
        [Route("")]
        [Route("{id}")]
        public async Task<IActionResult> Index(int? id)
        {
            var model = new DiscoveringPapersViewModel();
            if (!id.HasValue)
            {
                model.currentId = null;
                model.Papers = await _paperRepo.GetAllPublished();
                model.Categories = await _catRepo.GetAllAsync();

            }
            else
            {
                model.currentId = id;
                model.Papers = await _paperRepo.GetPublishedPapersForCategory(id.Value);
                model.Categories = await _catRepo.GetAllAsync();
            }
            return View(model);
        }

        [HttpGet]
        [Route("Submit")]
        public async Task<IActionResult> Submit()
        {
            var model = new PaperViewModel();
            model.LoadUserCategories(await _catRepo.GetAllAsync());
            return View(model);
        }

        [HttpPost]
        [Route("Submit")]
        public async Task<IActionResult> Submit(PaperViewModel model)
        {
            _service.InitializeModelState(this.ModelState);


            var user = await _usersRepo.GetUserByNameAsync(User.Identity.Name);
            var completed = await _service.AddPaperAsync(model, user.Id);
            if (completed)
                return RedirectToAction("index", "Home");

            model.LoadUserCategories(await _catRepo.GetAllAsync());
            return View(model);


        }


        [HttpGet]
        [Route("Accept")]
        public async Task<IActionResult> Accept()
        {
            var user = await _usersRepo.GetUserByNameAsync(User.Identity.Name);
            return View(await _paperRepo.GetUntakenAllPapers());

        }
        [HttpPost]
        [Route("Accept")]
        public async Task<IActionResult> Accept(int id)
        {
            var user = await _usersRepo.GetUserByNameAsync(User.Identity.Name);
            bool completed = await _service.AssignToEditor(user.Id, id);
            if (completed)
                return View(await _paperRepo.GetUntakenAllPapers());

            return BadRequest();
        }

        [HttpGet]
        [Route("Checkout/{id}")]
        public async Task<IActionResult> Checkout(int id)
        {
            return base.View(id);

        }
        [HttpPost]
        [Route("Checkout/{id}")]
        public async Task<IActionResult> Checkout(int id,string x)
        {
            return await Download(id);

        }
        [HttpGet]
        [Route("Download/{id}")]
        public async Task<IActionResult> Download(int id)
        {
            try
            {
                var paper = await _paperRepo.GetSinglePaper(id);
                paper.DownloadCounter += 1;
                var stream = _fileService.GetFileStream(paper.FinalPath);
                string filename = paper.FinalPath.Split('\\').Last();
                await _paperRepo.Edit(paper.Id, paper);
                return File(stream, "application/octet-stream", filename);
            }
            catch (Exception ex)
            {
                return BadRequest();

            }

        }

        [Route("View/{id}")]
        public async Task<IActionResult> View(int id)
        {
            try
            {
                var paper = await _paperRepo.GetSinglePaper(id);
                return View(paper);
            }
            catch (Exception ex) {
                return NotFound();
            }
        }


        [HttpGet]
        [Route("Track")]
        [Route("Track/{paperid}")]
        public async Task<IActionResult> Track(int? paperid)
        {

            var user = await _usersRepo.GetUserByNameAsync(User.Identity.Name);
            var model = new UserTrackViewModel();


            var papers = await _paperRepo.GetAllPapersOfUser(user.Id);
            var sorted = papers
                //   .Where(x => x.Status != "NEW" && x.Status != "PUBLISHED")
                .Where(x => x.Status != "NEW" )

                .OrderByDescending(x => x.Status == "PUBLSIHED")
                .ThenByDescending(x => x.Status == "WAITING EDIT")
                .ThenByDescending(x => x.Status == "UNDER REVIEW")
                .ThenByDescending(x => x.SubmissionDate)
                .ToList();
            if (paperid == null)
                paperid = 0;
            else
                model.Id = paperid.Value;
            model.AddPapers(sorted, paperid.Value);

            return View(model);

        }

        [HttpPost]
        [Route("Track/{paperid}")]
        public async Task<IActionResult> Track(int? paperid, IFormFile file)
        {

            var completed = await _service.UserUpdatePaperFile(paperid, file);
            if (!completed)
                return BadRequest();

            return RedirectToAction("Track", new { paperid = paperid.Value });

            

        }

    }

}
