﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Update;
namespace SubmissionSystem.Data
{
    public class ReviewsRepository : IReviewRepository, IDisposable
    {
        private SystemDbContext _context;
        public ReviewsRepository(SystemDbContext Context)
        {
            _context = Context;
        }
        public async Task Delete(int id)
        {
            var item = await _context.Reviews.SingleOrDefaultAsync(x => x.Id == id);
            if (item == null)
            {
                throw new KeyNotFoundException();
            }
            _context.Entry<Review>(item).State = EntityState.Deleted;
            await _context.SaveChangesAsync();

        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task Edit(int id, Review Review)
        {
            var item = await _context.Reviews.SingleOrDefaultAsync(x => x.Id == id);
            if (item == null)
            {
                throw new KeyNotFoundException();
            }
            _context.Entry<Review>(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }



        public async Task<IList<Review>> GetAllAsync()
        {
            return await _context.Reviews.ToListAsync();
        }



        public async Task<IList<Review>> GetAllReviewsForPaperAsync(int id)
        {
            return await _context.Reviews
                .Include(x => x.Paper)
                .Where(c => c.PaperId == id)
                .OrderBy(x => x.Date)
                .ToListAsync();
        }

        public async Task<IList<Review>> GetAllReviewsForPaperByReviewsAsync(int paperId, string ReviewerId, bool RemoveSystemMessage)
        {
            var result = _context.Reviews
                         .Include(x => x.Paper)
                         .Where(c => c.PaperId == paperId)
                         .Where(x => x.ReviewerId == ReviewerId);
            if (RemoveSystemMessage) 
            {
                result = result.Where(x => x.IsSystemMessage == false);
            }

            return await result.OrderBy(x => x.Date).ToListAsync();
        }

        public async Task<Review> GetSingleReviewAsync(int id)
        {

            var item = await _context.Reviews.SingleOrDefaultAsync(x => x.Id == id);
            if (item == null)
            {
                throw new KeyNotFoundException();
            }
            return item;
        }

        public async Task Post(Review Review)
        {
            _context.Reviews.Add(Review);
            await _context.SaveChangesAsync();

        }


    }
}
