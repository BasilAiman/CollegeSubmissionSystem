﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Data
{
    public class SystemSeedDbData
    {
        private IRoleRepository _repo;

        public SystemSeedDbData(IRoleRepository Repo)
        {
            _repo = Repo;
        }

        public async Task EnsureSeedData() {
         
            string[] roles = {"Admin","Editor","User","Reviewer-1","Reviewer-2","Reviewer-3" };

            foreach (var role in roles)
            {
                var identityrole = await _repo.GetRoleByNameAsync(role);
                if (identityrole == null)
                    await _repo.CreateAsync(new Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole(role));
            }

        }
    }
}
