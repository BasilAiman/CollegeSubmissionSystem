﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Data
{
    public interface IPaperRepository
    {
        Task<IList<Paper>> GetAllPapers();
        Task<IList<Paper>> GetUntakenAllPapers();
        Task<IList<Paper>> GetAllPapersOfUser(string UserId);
        Task<IList<Paper>> GetAllPublished();
        Task<IList<Paper>> GetPapersForCategory(int CategoryId);
        Task<IList<Paper>> GetPapersForEditor(string EditorId);
        Task<IList<Paper>> GetPapersForReviewer(string ReviewerId);
        Task<IList<Paper>> GetPublishedPapersForCategory(int CategoryId);
        Task<Paper> GetSinglePaper(int PaperId);
        Task Post(Paper Paper);
        Task Edit(int id, Paper Paper);
        Task Delete(int id);

    }
}
