﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SubmissionSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace SubmissionSystem.Data
{
    public class PaperRepository : IPaperRepository, IDisposable
    {
        public SystemDbContext _context;
        public PaperRepository(SystemDbContext Context)
        {
            _context = Context;
        }
        public async Task Delete(int id)
        {
            var item = await _context.Papers.SingleOrDefaultAsync(x => x.Id == id);
            if (item == null)
            {
                throw new KeyNotFoundException();
            }
            _context.Entry<Paper>(item).State = EntityState.Deleted;
            await _context.SaveChangesAsync();

        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task Edit(int id, Paper Paper)
        {
            var item = await _context.Papers.SingleOrDefaultAsync(x => x.Id == id);
            if (item == null)
            {
                throw new KeyNotFoundException();
            }
            _context.Entry<Paper>(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<IList<Paper>> GetAllPublished()
        {

            return await _context.Papers.Include(x => x.User).Where(x => x.Publish == true)
                .OrderByDescending(x => x.SubmissionDate)
                            .ToListAsync();

        }

        public async Task<IList<Paper>> GetAllPapers()
        {
            return await _context.Papers
                .Include(x => x.User)
                .Include(x => x.Editor)
                .OrderByDescending(x => x.SubmissionDate)
                .ToListAsync();
        }

        public async Task<IList<Paper>> GetPapersForCategory(int CategoryId)
        {
            return await _context.Papers
                            .Include(x => x.User)
                            .Include(x => x.Editor)
                            .Where(x => x.CategoryId == CategoryId)
                            .Where(x => x.Publish == true)
                            .OrderByDescending(x => x.SubmissionDate)
                            .ToListAsync();
        }
        public async Task<IList<Paper>> GetPublishedPapersForCategory(int CategoryId)
        {
            return await _context.Papers
                            .Include(x => x.User)
                            .Include(x => x.Editor)
                            .Where(x => x.CategoryId == CategoryId)
                            .Where(x => x.Publish)
                            .OrderByDescending(x => x.SubmissionDate)
                            .ToListAsync();
        }

        public async Task<IList<Paper>> GetPapersForEditor(string editorId)
        {
            return await _context.Papers
                                   .Include(x => x.User)
                                   .Include(x => x.Editor)
                                   .Where(x => x.EditorId == editorId)
                                   .OrderByDescending(x => x.SubmissionDate)
                                   .ToListAsync();
        }

        public async Task<IList<Paper>> GetPapersForReviewer(string ReviewerId)
        {
            var papers = await  _context.Papers
                .Where(
                    x => x.LevelOneReviewer == ReviewerId ||
                    x.LevelTwoReviewer == ReviewerId ||
                    x.LevelThreeReviewer == ReviewerId).ToListAsync();
            return papers;



        }

        public async Task<Paper> GetSinglePaper(int PaperId)
        {
            var item = await _context.Papers.Include(x => x.Category).Include(x => x.User).SingleOrDefaultAsync(x => x.Id == PaperId);
            if (item == null)
            {
                throw new KeyNotFoundException();
            }
            return item;
        }

        public async Task Post(Paper Paper)
        {
            _context.Papers.Add(Paper);
            await _context.SaveChangesAsync();
        }

        public async Task<IList<Paper>> GetAllPapersOfUser(string UserId)
        {
           return await _context.Papers.Where(x => x.UserId == UserId).ToListAsync();
        }

        public async Task<IList<Paper>> GetUntakenAllPapers()
        {
            return await _context.Papers
                .Where(x => x.isTaken == false)
                .OrderByDescending(x => x.SubmissionDate)
                .ToListAsync();
        }
    }
}
