﻿using System.Collections;
using System.Linq;
using SubmissionSystem.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Security.Claims;
using SubmissionSystem.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace SubmissionSystem.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<ApplicationUser> _manager;

        public UserRepository(UserManager<ApplicationUser> userManager, UserManager<ApplicationUser> Store)
        {

            _manager = userManager;
        }

        public async Task<ApplicationUser> GetUserByNameAsync(string username)
        {
            return await _manager.FindByNameAsync(username);
        }

        public async Task<IEnumerable<ApplicationUser>> GetAllUsersAsync()
        {
            return await _manager.Users.ToArrayAsync();
        }

        public async Task CreateAsync(ApplicationUser user, string password)
        {
            var IdentityResult = await _manager.CreateAsync(user, password);
            if (!IdentityResult.Succeeded)
            {
                string message = String.Join("\n", IdentityResult.Errors.Select(x => x.Description));
                throw new Exception(message);
            }
        }

        public async Task DeleteAsync(ApplicationUser user)
        {
            await _manager.DeleteAsync(user);
        }

        public async Task UpdateAsync(ApplicationUser user)
        {
            await _manager.UpdateAsync(user);

        }




        public async Task AddUserToRoleAsync(ApplicationUser user, string role)
        {
            await _manager.AddToRoleAsync(user, role);

        }
        public async Task<IEnumerable<string>> GetRolesForUserAsync(ApplicationUser user)
        {
            return await _manager.GetRolesAsync(user);
        }
        public async Task RemoveUserFromRolesAsync(ApplicationUser user, params string[] roleNames)
        {
            await _manager.RemoveFromRolesAsync(user, roleNames);
        }
        public async Task ChangePasswordAsync(ApplicationUser user, string currentPassword, string newPassword)
        {
            
                await _manager.ChangePasswordAsync(user, currentPassword, newPassword);
            
         
        }

        private bool _disposed = false;
        public void Dispose()
        {
            if (!_disposed)
            {
                _manager.Dispose();
                _manager.Dispose();
            }

            _disposed = true;
        }

        public async Task<bool> VerifyUserPassword(ApplicationUser user, string providedPassword)
        {
            return await _manager.CheckPasswordAsync(user, providedPassword);
        }

        public async Task<IEnumerable<ApplicationUser>> GetAllUsersInRoleAsync(string Role)
        {
            return await _manager.GetUsersInRoleAsync(Role);
        }

        public async Task<ApplicationUser> GetUserById(string id)
        {
            return await _manager.FindByIdAsync(id);
        }

        public async Task<IEnumerable<ApplicationUser>> GetAllEditors()
        {
            return await _manager.GetUsersInRoleAsync("Editor");       
        }

        public async Task<IEnumerable<ApplicationUser>> GetAllReviewer()
        {
            var result = new List<ApplicationUser>();
            result.AddRange(await _manager.GetUsersInRoleAsync("Reviewer-1"));
            result.AddRange(await _manager.GetUsersInRoleAsync("Reviewer-2"));
            result.AddRange(await _manager.GetUsersInRoleAsync("Reviewer-3"));
            return result;
        }
    }
}