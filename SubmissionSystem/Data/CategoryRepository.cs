﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SubmissionSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace SubmissionSystem.Data
{
    public class CategoryRepository : ICategoryRepository,IDisposable
    {
        private SystemDbContext _context;
        public CategoryRepository(SystemDbContext Context)
        {
            _context = Context;
        }
        public async Task<bool> CheckIfCategoryWithNameExist(string name) {
            return await _context.Categories.AnyAsync(x => x.Name.ToLower() == name.ToLower());
        }
        public async Task Delete(int Id)
        {
            var Category = await _context.Categories.SingleOrDefaultAsync(x => x.Id == Id);
            if (Category == null)
                throw new KeyNotFoundException();
            _context.Categories.Remove(Category);
            await _context.SaveChangesAsync();

        }


        public async Task Edit(int id, Category Category)
        {
            if (!_context.Categories.Any(x => x.Id == id))
                throw new KeyNotFoundException();

            _context.Entry(Category).State = EntityState.Modified;

            await _context.SaveChangesAsync();

        }

        public async Task<IList<Category>> GetAllAsync()
        {
            return await _context.Categories
                .Include(x => x.Papers)               
                .ToListAsync();
        }

        public async Task<Category> GetSingleAsync(int id)
        {
            var Category = await _context.Categories.Include(X => X.Papers).SingleOrDefaultAsync(x => x.Id == id);
            if (Category == null)
                throw new KeyNotFoundException();

            return Category;
        }

        public async Task Post(Category Category)
        {
            _context.Categories.Add(Category);
            await _context.SaveChangesAsync();

        }



        public void Dispose()
        {
            _context.Dispose();
        }

        public async  Task<Category> GetCategoryByName(string selectedCategory)
        {
            return await _context.Categories.SingleOrDefaultAsync(x => x.Name.ToLower() == selectedCategory.ToLower());

        }
    }
}
