﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SubmissionSystem.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace SubmissionSystem.Data
{
    public class RoleRepository : IRoleRepository
    {
        private readonly IRoleStore<IdentityRole> _store;
        private readonly RoleManager<IdentityRole> _manager;

        public RoleRepository(IRoleStore<IdentityRole> Store,RoleManager<IdentityRole> Manager)
        {
            _store = Store;
            _manager = Manager;
        }

        public async Task<IdentityRole> GetRoleByNameAsync(string name)
        {
            return await _store.FindByNameAsync(name,new System.Threading.CancellationToken(false));
        }

        public async Task<IEnumerable<IdentityRole>> GetAllRolesAsync()
        {
            return await _manager.Roles.ToArrayAsync();
        }

        public async Task CreateAsync(IdentityRole role)
        {
            await _manager.CreateAsync(role);
        }

        private bool _disposed = false;
        public void Dispose()
        {
            if (!_disposed)
            {
                _store.Dispose();
                _manager.Dispose();
            }

            _disposed = true;
        }
    }
}