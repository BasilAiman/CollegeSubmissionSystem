﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Data
{
    public interface ICommentsRepository
    {
        Task<IList<Comment>> GetAllAsync();
        Task<IList<Comment>> GetCommentsForPaperAsync(int paperid);
        Task<Comment> GetSingleCommentAsync(int id);
        Task Post(Comment Comment);
        Task Edit(int id,Comment Comment);
        Task Delete(int id);

    }
}
