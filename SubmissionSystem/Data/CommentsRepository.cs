﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Update;
namespace SubmissionSystem.Data
{
    public class CommentsRepository : ICommentsRepository, IDisposable
    {
        private SystemDbContext _context;
        public CommentsRepository(SystemDbContext Context)
        {
            _context = Context;
        }
        public async Task Delete(int id)
        {
            var item = await _context.Comments.SingleOrDefaultAsync(x => x.Id == id);
            if (item == null)
            {
                throw new KeyNotFoundException();
            }
            _context.Entry<Comment>(item).State = EntityState.Deleted;
            await _context.SaveChangesAsync();

        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task Edit(int id,Comment Comment)
        {
            var item = await _context.Comments.SingleOrDefaultAsync(x => x.Id == id);
            if (item == null)
            {
                throw new KeyNotFoundException();
            }
            _context.Entry<Comment>(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public async Task<IList<Comment>> GetAllAsync()
        {
            return await _context.Comments.ToListAsync();
        }

        public async Task<IList<Comment>> GetCommentsForPaperAsync(int id)
        {
            return await _context.Comments
                .Include(x => x.Paper)
                .Where(c => c.PaperId == id)
                .OrderBy(x => x.Date)
                .ToListAsync();
        }

        public async Task<Comment> GetSingleCommentAsync(int id)
        {

            var item = await _context.Comments.SingleOrDefaultAsync(x => x.Id == id);
            if (item == null)
            {
                throw new KeyNotFoundException();
            }
            return item;
        }

        public async Task Post(Comment Comment)
        {
            _context.Comments.Add(Comment);
            await _context.SaveChangesAsync();

        }
    }
}
