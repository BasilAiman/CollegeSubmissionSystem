﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Data
{
    public interface IReviewRepository
    {
        Task<IList<Review>> GetAllAsync();
        Task<IList<Review>> GetAllReviewsForPaperAsync(int paperId);
        Task<IList<Review>> GetAllReviewsForPaperByReviewsAsync(int paperId,string ReviewerId, bool RemoveSystemMessage);
        Task<Review> GetSingleReviewAsync(int id);
        Task Post(Review Review);
        Task Edit(int id, Review Review);
        Task Delete(int id);
    }
}
