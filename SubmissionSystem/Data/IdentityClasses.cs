﻿using Microsoft.AspNetCore.Identity;
using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace SubmissionSystem.Data
{
    public class SystemSigninManager : SignInManager<ApplicationUser>
    {
        public SystemSigninManager(UserManager<ApplicationUser> userManager,
            IHttpContextAccessor contextAccessor, 
            IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory,
            IOptions<IdentityOptions> optionsAccessor,
            ILogger<SignInManager<ApplicationUser>> logger) : 
            base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger)
        {
        }
    }

}
