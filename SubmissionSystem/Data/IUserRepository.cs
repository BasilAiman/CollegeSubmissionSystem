﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Claims;
using SubmissionSystem.Models;

namespace SubmissionSystem.Data
{
    public interface IUserRepository : IDisposable
    {

        Task<ApplicationUser> GetUserByNameAsync(string username);
        Task<ApplicationUser> GetUserById(string id);
        Task<IEnumerable<ApplicationUser>> GetAllUsersAsync();
        Task<IEnumerable<ApplicationUser>> GetAllUsersInRoleAsync(string Role);
        Task<IEnumerable<ApplicationUser>> GetAllEditors();
        Task<IEnumerable<ApplicationUser>> GetAllReviewer();
        Task CreateAsync(ApplicationUser user, string password);
        Task DeleteAsync(ApplicationUser user);
        Task UpdateAsync(ApplicationUser user);
        Task<bool> VerifyUserPassword(ApplicationUser user, string providedPassword);
        Task AddUserToRoleAsync(ApplicationUser newUser, string selectedRole);
        Task<IEnumerable<string>> GetRolesForUserAsync(ApplicationUser user);
        Task RemoveUserFromRolesAsync(ApplicationUser user, params string[] roleNames);
        Task ChangePasswordAsync(ApplicationUser user, string currentPassword, string newPassword);
    }
}