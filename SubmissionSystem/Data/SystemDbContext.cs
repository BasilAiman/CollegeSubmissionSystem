﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Data
{
    public class SystemDbContext : IdentityDbContext
    {
        public SystemDbContext(DbContextOptions<SystemDbContext> options):base(options)
        {

        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Paper> Papers { get; set; }
        public virtual DbSet<Review> Reviews { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<FollowCategory>().HasKey(t => new { t.CategoryId, t.UserId });
            builder.Entity<FollowCategory>().HasOne(x => x.User).WithMany(x => x.FollowingCategories).HasForeignKey(x => x.UserId);
            builder.Entity<FollowCategory>().HasOne(x => x.Category).WithMany(x => x.FollowingUsers).HasForeignKey(x => x.CategoryId);
        }





    }
}
