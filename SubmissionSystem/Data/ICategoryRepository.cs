﻿using SubmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubmissionSystem.Data
{
    public interface ICategoryRepository
    {
        Task<IList<Category>> GetAllAsync();
        Task<bool> CheckIfCategoryWithNameExist(string name);
        Task<Category> GetSingleAsync(int id);
        Task Post(Category Category);
        Task Edit(int id,Category Category);
        Task Delete(int Id);
        Task<Category> GetCategoryByName(string selectedCategory);
    }
}
